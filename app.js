const electron = require('electron')
const app = electron.app
const path = require('path')
const ipcMain = electron.ipcMain
const BrowserWindow = electron.BrowserWindow

let url
if (process.env.NODE_ENV === 'DEV') {
  url = 'http://localhost:8080/'
} else {
  url = `file://${process.cwd()}/dist/index.html`
}

app.on('ready', () => {
    let window = new BrowserWindow({
        width: 1000,
        height: 900,
        minWidth: 480,
        show: false,
        webPreferences: {
          nodeIntegration: false, // is default value after Electron v5
          contextIsolation: true, // protect against prototype pollution
          enableRemoteModule: false, // turn off remote
          preload: path.join(__dirname, "preload.js") // use a preload script
        }
    })

    // and load the url of the app.
    window.loadURL(url)

    window.once('ready-to-show', () => {
        window.show()
    })
})
ipcMain.on('ping', (event, data) => {
    var python = require('child_process').spawn('python', ['./hello.py']);
    python.stdout.on('data',function(data){
        console.log("data: ",data.toString('utf8'));
        var output = data.toString('utf8');
        event.sender.send('pong' , output);
    });
    
})