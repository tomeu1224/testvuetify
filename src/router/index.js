import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Tareas from '../views/Tareas.vue'
import TareasCrud from '../views/Tareas-crud.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  // {
  //   path: '/about',
  //   name: 'about',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // },
  {
    path: '/tareas',
    name: 'tareas',
    component: Tareas
  },
  {
    path: '/tareas-crud',
    name: 'tareas-crud',
    component: TareasCrud
  }
]

const router = new VueRouter({
  routes
})

export default router
